#' -------------------------------------------------------------------------------
#' 
#' Part 1 Assignemnt
#' @author Daniel Stratti
#' @description 
#' 1. Data preparation and cleaning (10 points). Write an R script preparation.R
#' that prepares the data for next task including replaces the missing values 
#' with the respective column medians of the training data set. Your R script 
#' should execute the following steps:
#' -------------------------------------------------------------------------------
#' -------------------------------------------------------------------------------
#' 
#' A) Load the entire dataset, split into 3 subsets for training, validation and 
#' test (choose sensible split proportions).
#' -------------------------------------------------------------------------------
profit <- read.csv("./Q1/profitNA.csv")
head(profit)
dim(profit)

#' 70:15:15 Was chosen due to the low row count
N <- nrow(profit)
NTrain <- round(N * 0.7)
NVal <- round(N * 0.15)
NTest <- N - NTrain - NVal

# Set seed and select random sample for training, testing and validation 
set.seed(312)
idx <- sample(N)

profit_train <- profit[idx[1:NTrain],]
profit_val <- profit[idx[(NTrain+1):NVal],]
profit_test <- profit[idx[(NTrain+NVal+1):N],]


#' B) Take the training data, and for each column except column CR and DCR, 
#' compute a separate column median.
#' -------------------------------------------------------------------------------
sapply(profit_train, class)


#' Function to extract the median value ecah numeric column in a data frame
#' @param data {DataFrame}: The data to extract column wise medians from
#' @param exclude {Vector}: A vector of column names to exclude
#' 
#' @return {List}: A list of medians with the column name as the keys
getMedians <- function(data, exclude) {
  medians <- list()
  
  for (name in colnames(data)) {
    
    if (class(data[,name]) %in% c("numeric", "integer") ) {
      
      if (!(name %in% exclude)) {
        medians[[ name ]] <-  median(data[,name], na.rm = T)
      }
    }
  }
  
  return(medians)
}

#' Extract Medains of each column but 'CR' and 'DCR'
medians <- getMedians(profit_train, c("CR", "DCR"))
names(medians)

#' C) Replace all missing values in training with the respective column median 
#' computed in above step except column CR and DCR.
#' -------------------------------------------------------------------------------

#' Function to fill NA values with a a provided list of values 
#' @param data {DataFrame}: The data frame to fill column wise of median values
#' @param toFill {List}: The list of median values with colnames as keys
#' @return {DataFrame}: the filled data frame
fillNAs <- function(data, toFill) {
  for (key in names(toFill)) {
    
    if (key %in% colnames(data)) {
      
      data[is.na(data[,key]), key] <- toFill[[key]]
    }
  }
  return(data)
}

# Fill NA's
profit_train_med <- fillNAs(profit_train, medians)

profit_train[is.na(profit_train[,'DIV3']), 'DIV3']
profit_train_med[is.na(profit_train[,'DIV3']), 'DIV3']


#' D) Repeat these steps for the validation and test datasets, but instead of
#' computing new column medians, use the column medians that you computed using 
#' the training data, except column CR and DCR
#' -------------------------------------------------------------------------------
profit_val_med <- fillNAs(profit_val, medians)
profit_test_med <- fillNAs(profit_test, medians)

write.csv(profit_train_med, file="./Q1/profit_train_med.csv", row.names = F)
write.csv(profit_val_med, file="./Q1/profit_val_med.csv", row.names = F)
write.csv(profit_test_med, file="./Q1/profit_test_med.csv", row.names = F)
#' -------------------------------------------------------------------------------
#' 
#' Part 1 Assignemnt
#' @author Daniel Stratti
#' @description 
#' 4. Find out which independent variables best predict your chosen output 
#' variable using LASSO regression (25 points). Create an R script 
#' variableselection.R to do the following.
#' -------------------------------------------------------------------------------
#' -------------------------------------------------------------------------------
#' 
#' (a) Combine training set and validation set together called combined set.
#' -------------------------------------------------------------------------------
library(tidyverse)

profit_train <- read.csv("./Q1/profit_train.csv")
profit_val <- read.csv("./Q1/profit_val.csv")
profit_test <- read.csv("./Q1/profit_test.csv")

combined_set <- rbind(profit_test, profit_val)

#'
#' (b) Apply LASSO regression on the combined set with cross validation to 
#' select the best λ for l1 regularisation. (Hint: use glmnet package to do it. )
#' -------------------------------------------------------------------------------
library(glmnet)


#' Function to standardise column values
#' @param data {DataFrame}: The data frame to standardise columns
#' @param exclude {Vector}: A vector of column names to exclude
#' 
#' @return {List}: A list of medians with the column name as the keys
standardCols <- function(data, exclude) {
  
  for (name in colnames(data)) {
    
    if (class(data[,name]) %in% c("numeric", "integer") ) {
      
      if (!(name %in% exclude)) {
        data[,name] <-  (data[,name] - mean(data[,name]))/sd(data[,name])
      }
    }
  }
  
  return(data)
}

combined_set_std <- standardCols(combined_set[,-1], c('Firm'))
x <- as.matrix(select(combined_set_std, -PE, -PT))
y <- as.matrix(select(combined_set_std, PE))
lasso <- cv.glmnet(x, y, alpha=1)

lasso$lambda.min
plot(lasso)

coef(lasso, s = "lambda.min")

#' 
#' (c) Take the variables selected by the best λ as predictors and your chosen 
#' output variable,fit a simple linear model with least squares on the training 
#' set, and obtain the error/loss on validation set and test set.
#' 

fit.1 <- lm(PE ~ CR + DCR + AS + CAS, data=profit_train)

#' Calculates the Root Mean Squared Error (RMSE) from a model
#' @param model {lm}: The linear model to extract residuals from
#' 
#' @return {numeric}: The RMSE of the models residuals
rmseResiduals <- function(model) {
  return(
    sqrt(sum(residuals(model)^2)/length(residuals(model)))
  )
}


#' Calculates the Root Mean Squared Error (RMSE) from a models predictions vs test values
#' @param model {lm}: The linear model to predict values from the `test_set`
#' @param test_set {data.frame}: The data frame containig values to form predictions
#' @param target {character}: The column name in the `test_set` of the target variable 
#' 
#' @return {numeric}: The RMSE of the models predictions vs test values
rmseValidation <- function(model, test_set, target) {
  pred <- predict(model, test_set)
  return(
    sqrt(sum((pred - test_set[,target])^2)/nrow(test_set))
  )
}

rmseResiduals(fit.1)
rmseValidation(fit.1, profit_val, "PE")
rmseValidation(fit.1, profit_test, "PE")

#' 
#' (d) Compare the model generated above to the best one obtained from previous 
#' task. Draw barplot to compare two models error/loss on validation set and test 
#' set. Consult R help page on barplot() function for bar plot.

load("./Q1/lm_PE_v_CAS.rda") # model name lm

rmse_models <- c(
  rmseValidation(fit.1, profit_val, "PE"),
  rmseValidation(fit.1, profit_test, "PE"),
  rmseValidation(lm, profit_val, "PE"),
  rmseValidation(lm, profit_test, "PE")
)
names(rmse_models) <- c(
  "Lasso-Val",
  "Lasso-Test",
  "S-Val",
  "S-Test"
)
barplot(
  rmse_models,
  main="Lasso Vs Selected",
  xlab="Models",
  ylab="RMSE",
  col = c(2,1,2,1)
)

#' As shown in the barplot, the orriginally selected model PE ~ CAS out performs the lasso model on
#' The test set, however slightly under performs on the validation set. Based off this I would 
#' recommend taking the PE ~ CAS over teh lasso regeression model as it is quicker to compute with 
#' less preprocessing (standardisation) and performs very similarly.


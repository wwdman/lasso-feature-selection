#' -------------------------------------------------------------------------------
#' 
#' Part 2 Assignemnt
#' @author Daniel Stratti
#' @description 
#' You are given a data set in chem.csv about a chemical test. There are four 
#' predictors, X.1 - X.4 in the data frame. X.1 is actually a factor with 3 levels 
#' and others are numeric or integer. y is the response. Interestingly there is a 
#' variable called probs associated with y, which is used to indicate how sure 
#' that sample is classified as positive. 
#' 
#' For example, in the first observation, i.e. the first row in the data frame, y 
#' is Negative, probs is 0.01 which means the data collector is 1% sure that this 
#' classification is positive, or in other words, 100%-1%=99% sure that this is 
#' negative. While in the third observation, probs is 0.23 meaning 67% sure about 
#' this negativity. Write an R script probs.R to build a model for this data to 
#' make prediction and claim your model performance.
#' -------------------------------------------------------------------------------
#' -------------------------------------------------------------------------------
#' 


#' Exploration & Clensing
#' -------------------------------------------------------------------------------
chems <- read.csv('./Q2/chem.csv')
head(chems)
dim(chems)

# Convert data Types
sapply(chems, class)
#chems$X.1 <- factor(chems$X.1)
chems$y <- factor(chems$y)
chems$target <- ifelse(chems$y == 'Positive', 1, 0)
sapply(chems, class)

# Make sure there are no NA's, all should output 0
sum(is.na(chems$X.1))
sum(is.na(chems$X.2))
sum(is.na(chems$X.3))
sum(is.na(chems$X.4))
sum(is.na(chems$y))
sum(is.na(chems$probs))

# observe the data
plot(chems, col=chems$y)
## X.2, X.3, X.4 look to be the best predictors with X.3 being the best copared to all


#' Train/Validation/Test Split
#' -------------------------------------------------------------------------------
#' 70:15:15 Was chosen due to the low row count
N <- nrow(chems)
NTrain <- round(N * 0.7)
NVal <- round(N * 0.15)
NTest <- N - NTrain - NVal

# Set seed and select random sample for training, testing and validation 
set.seed(312)
idx <- sample(N)

# Split the data into test & training sets
chem_train <- chems[idx[1:NTrain],]
chem_test <- chems[idx[(NTrain+1):NVal],]
chem_val <- chems[idx[(NTrain+NVal+1):N],]
colnames(chem_val)


#' Model Creation (Logistic Regression)
#' -------------------------------------------------------------------------------
# Make Logistic Regression Models with each combination of factors
fit.1 <- glm(target ~ X.1, data = chem_train, family = binomial(link="logit"))
fit.2 <- glm(target ~ X.2, data = chem_train, family = binomial(link="logit"))
fit.3 <- glm(target ~ X.3, data = chem_train, family = binomial(link="logit"))
fit.4 <- glm(target ~ X.4, data = chem_train, family = binomial(link="logit"))

fit.5 <- glm(target ~ X.1 + X.2, data = chem_train, family = binomial(link="logit"))
fit.6 <- glm(target ~ X.1 + X.3, data = chem_train, family = binomial(link="logit"))
fit.7 <- glm(target ~ X.1 + X.4, data = chem_train, family = binomial(link="logit"))
fit.8 <- glm(target ~ X.2 + X.3, data = chem_train, family = binomial(link="logit"))
fit.9 <- glm(target ~ X.2 + X.4, data = chem_train, family = binomial(link="logit"))
fit.10 <- glm(target ~ X.3 + X.4, data = chem_train, family = binomial(link="logit"))

fit.11 <- glm(target ~ X.1 + X.2 + X.3, data = chem_train, family = binomial(link="logit"))
fit.12 <- glm(target ~ X.1 + X.2 + X.4, data = chem_train, family = binomial(link="logit"))
fit.13 <- glm(target ~ X.1 + X.3 + X.4, data = chem_train, family = binomial(link="logit"))
fit.14 <- glm(target ~ X.2 + X.3 + X.4, data = chem_train, family = binomial(link="logit"))

fit.15 <- glm(target ~ X.1 + X.2 + X.3 + X.4, data = chem_train, family = binomial(link="logit"))


models <- list(
  fit.1, # AIC: 960.77
  fit.2, # AIC: 922.94
  fit.3, # AIC: 516.36
  fit.4, # AIC: 957.55
  
  fit.5, # AIC: 909.12
  fit.6, # AIC: 485.66
  fit.7, # AIC: 944.89
  fit.8, # AIC: 410.12
  fit.9, # AIC: 909.57
  fit.10, # AIC: 487.45
  
  fit.11, # AIC: 345.14
  fit.12, # AIC: 894.43
  fit.13, # AIC: 452.43
  fit.14, # AIC: 374.12
  
  fit.15 # AIC: 299.58
)
#' Occording the the AIC with one predictor X.3 produces the most desirable loss - parameter outcome
#' With two predictors X.2 and X.3 produces the most desirable loss - parameter outcome
#' With three predictors X.1, X.2 and X.3 produces the most desirable loss - parameter outcome

names(models) <- 1:15

for (key in names(models)) {
  print(summary(models[[key]]))
}


#' Model Selection
#' -------------------------------------------------------------------------------
# ROC

#' Function to calculate the reciever operating characteristic (ROC) curve
#' @param models {List}: The list of logistic regression models to compare
#' @param test_set {Dataframe}: The data frame of values to test
#' @param target {String}: The name of the target variable
#' @param steps {Integer}: The number of steps along the ROC curve to calculate
#' 
#' @return {List}: A list of values for each models ROC curve
modelROC <- function(models, test_set, target, steps=11) {
  threshold <- seq(0,1,length.out = steps)
  modelsROC <- list()
  
  for (key in names(models)) {
    values <- data.frame(thresh=0,fpr=0, tpr=0, sas=0)
    pre <- predict(models[[key]], test_set, type = "response")
    
    for (i in 1:steps) {
      prex <- ifelse(pre < threshold[i], 0, 1)
      values[i, "thresh"] <- threshold[i]
      values[i, "fpr"] <- sum((prex==1)*(test_set[,target]==0)) / sum(test_set[,target] == 0)
      values[i, "tpr"] <- sum((prex==1)*(test_set[,target]==1)) / sum(test_set[,target] == 1)
      values[i, "sas"] <- (1-values[i, "fpr"]) + values[i, "tpr"]
    }
    
    modelsROC[[key]] <- values
  }
  
  return(modelsROC)
}

# Calculate ROC values for optimal thresholds & model comparison
models_roc <- modelROC(models, chem_val, 'target', steps=101)

# plot the ROC's
plot(
  x=seq(0,1,length.out = 10), 
  y=seq(0,1,length.out = 10), 
  type='n',
  xlab="1 - Specificity",
  ylab="Sensitivity", 
  main="ROC Curve")


for (key in names(models_roc)) {
  # Add each ROC
  points(
    x=models_roc[[key]]$fpr, 
    y=models_roc[[key]]$tpr,
    type='l',
    col=key)
  
  # Add the best threshold for each
  i <- which.max(models_roc[[key]]$sas)
  points(
    models_roc[[key]]$fpr[i], 
    models_roc[[key]]$tpr[i], 
    pch=key,
    col=key)
  
  # Label the threshold
  text(models_roc[[key]]$fpr[i], models_roc[[key]]$tpr[i],
       paste("Model ", key, " = ", signif(models_roc[[key]]$thresh[i],2), " | ", signif(models_roc[[key]]$sas[i],2)), pos=1)
}

#' Based off the above the top 3 performing models are: 
#' Model 15: threshold = 0.24, sas = 1.8
#' Model 11: threshold = 0.54, sas = 1.8
#' Model 14: threshold = 0.54, sas = 1.7

# Top models
plot(
  x=seq(0,1,length.out = 10), 
  y=seq(0,1,length.out = 10), 
  type='n',
  xlab="1 - Specificity",
  ylab="Sensitivity", 
  main="ROC Curve")

for (key in c(15, 11, 14)) {
  # Add each ROC
  points(
    x=models_roc[[key]]$fpr, 
    y=models_roc[[key]]$tpr,
    type='l',
    col=key)
  
  # Add the best threshold for each
  i <- which.max(models_roc[[key]]$sas)
  points(
    models_roc[[key]]$fpr[i], 
    models_roc[[key]]$tpr[i], 
    pch=key,
    col=key)
  
  # Label the threshold
  text(models_roc[[key]]$fpr[i], models_roc[[key]]$tpr[i],
       paste("Model ", key, " = ", signif(models_roc[[key]]$thresh[i],2), " | ", signif(models_roc[[key]]$sas[i],2)), pos=1)
}

# Confusion Matrix
library(caret)
#' Model 15: threshold = 0.24, sas = 1.8
#' Model 11: threshold = 0.54, sas = 1.8
#' Model 14: threshold = 0.54, sas = 1.7
thresholds <- setNames(as.list(c(0.24, 0.54, 0.54)), c(15,11,14))

for (key in names(thresholds)) {
  cat(rep('-', 40))
  print('\n')
  print(key)
  pre <- predict(models[[key]], chem_test, type = "response")
  prex <- ifelse(pre < thresholds[[key]], 0, 1)
  cfm <- confusionMatrix(as.factor(prex),as.factor(chem_test$target))
  print(cfm)
  cat(rep('-', 40))
  print('\n')
}

#' Final Model Performance
#' -------------------------------------------------------------------------------
#' Based on the above output and the assumption that for  this  problem it would be more desirable to
#' have a false negative than a false positive, Model 11 is the final chosen model with formula: 
#'      target ~ X.1 + X.2 + X.3

# Show Accuracy on test set
pre <- predict(models[[11]], chem_test, type = "response")
prex <- ifelse(pre < thresholds[['11']], 0, 1)
cfm <- confusionMatrix(as.factor(prex),as.factor(chem_test$target))
print(cfm)

#' Based on the performance of the test set the accuracy of the model is
#' Overall Acc: 0.88
#' Positive Acc: 0.87
#' Negative Acc: 0.89
glm_11 <- models[[11]]
save(glm_11, file = "./Q2/glm_11_X.1_X.2_X.3.rda")


#' Function to fill rows of the original chem.csv table based on new data and the above model
#' @param new_data {Dataframe}: The data frame contain values to predict from.
#'       NOTE* column names mus be as expected: X.1, X.2, X.3, X.4
#' 
#' @return {Dataframe}: Returns the `new_data` with two new columns: y (Positive/Negative), probs
predClaim <- function(new_data) {
  load("./Q2/glm_11_X.1_X.2_X.3.rda")
  pre <- predict(glm_11, new_data, type = "response")
  prex <- ifelse(pre < thresholds[['11']], 'Negative', 'Positive')
  
  probs = pre
  for(i in length(probs)) {
    probs[i] <- round(probs[i], 2)
  }
  
  new_data$y = prex
  new_data$probs = probs
  return(new_data)
}

predClaim(chems[4,])

# LASSO Feature Selection 

This project explores using pre-processing techniques, regularisation & normalisations methods to identify features and produce an optimal linear model. The project explores profit data of a firm from the 1960s and several independent variables


## Data Dictionary
The file `./Q1/profitNA.csv` is the initial data used within this project and consist of the folowing factors:

### Dependent Variables
```
PT 		(Net Income + Intrest Expense) / total assets
PE 		Net Income / Shareholder equity
```
### Independent Variables
```
CR		Weighted Concentration Ratio of firm's production markets
DCR		A variable related to CR somehow but contains only 0 and 1
AS 		weighted average industry advertising-to-sales ratio of firm's product markets
CAS		Overall advertising-to-sales ration of the firm
GR		Weighted average percent changes in industry sales in the firm's markets
DIV2		Firm's diversification in 2-digit (more broad) industries
DIV3		Firm's diversification in 3-digit (less broad) industries
SIZ		Firm's 1968 Total assets ($1M)
LRSIZ		1/ln(SIZ)
```

## Preperation
The script `preperation.R` cleans & splits the dataset then populates missing values with the respective column medians from the test set

The script `fillingCRDCR.R` is then used to model `CR` & `DCR` and populate its missing data. The data should now be fully cleaned and ready to model the dependent variables

## Modelling
The script `linreg.R` explores several linear models containing various parameters to predict the dependent variables `PT & PE` 

The script `variableselection.R` then creates a LASSO regression model to explore the most relevant features contributing to the dependent variables. The optimal model produced via LASSO regularisation is then compared againts the top performing linear regression model.

## Additional Project
Logistic regression is explored in the script `probs.R` to predict the effectiveness of a drug based off observational evidence provided in `./Q2/chem.csv`